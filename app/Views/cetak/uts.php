<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <title></title>
    </head>
    <body>

        <table border="1" width="100%" style="border-collapse:collapse">
            <tr style="border:1px solid;">
                <td style="border:0px solid;padding:10px; width: 20%;"><img style="width:40px; height:40px" src="<?= $image ?>"/></td>
                <td style="border:0px solid;text-align:center; font-size:16px; font-weight:bold">KARTU UJIAN TENGAH SEMESTER</td>
                <td style="border:0px solid;padding:10px; width: 20%; text-align: center;"><p style="padding:0 30px;border:2px solid;">TA 2019/2020 </p></td>
            </tr>

            <tr>
        		<td colspan="3" style="padding:10px; border:1px solid">

        		    <table  border="0" width="100%" style="border-collapse:collapse; margin-bottom:10px;">
                        <tr>
                            <td style="font-size:12px; padding:4px">Nama &thinsp;&thinsp;&thinsp; &thinsp;&thinsp;&thinsp;&thinsp;&thinsp;&thinsp;&thinsp; : &thinsp;&thinsp;&thinsp;&thinsp;&thinsp; <?= $data->nama ?></td>
                            <td style="font-size:12px; padding:4px">Kelas &thinsp;&thinsp;&thinsp;&thinsp;&thinsp;&thinsp;&thinsp;&thinsp;&thinsp;&thinsp;&thinsp;&thinsp;&thinsp;&thinsp;&thinsp;&thinsp;&thinsp;&thinsp;&thinsp;&thinsp;&thinsp;&thinsp;&thinsp;&thinsp;&thinsp;&thinsp;&thinsp;&thinsp; : &thinsp;&thinsp;&thinsp;&thinsp;&thinsp; <?= $data->kelas ?> </td>

                        </tr>
                        <tr>
                            <td style="font-size:12px; padding:4px">NIM  &thinsp;&thinsp;&thinsp;&thinsp;&thinsp;&thinsp;&thinsp;&thinsp;&thinsp;&thinsp;&thinsp;&thinsp;&thinsp;&thinsp;&thinsp; : &thinsp;&thinsp;&thinsp;&thinsp;&thinsp; <?= $data->nim ?></td>
                            <td style="font-size:12px; padding:4px">Semester &thinsp;&thinsp;&thinsp;&thinsp;&thinsp;&thinsp;&thinsp;&thinsp;&thinsp;&thinsp;&thinsp;&thinsp;&thinsp;&thinsp;&thinsp;&thinsp;&thinsp;&thinsp; : &thinsp;&thinsp;&thinsp;&thinsp;&thinsp; <?= $data->semester ?> </td>
                        </tr>
        				<tr>
        					<td style="font-size:12px; padding:4px; width:50%;">Fakultas &thinsp;&thinsp;&thinsp;&thinsp;&thinsp; : &thinsp;&thinsp;&thinsp;&thinsp;&thinsp; Ilmu Komputer</td>
        					<td style="font-size:12px; padding:4px">Program Studi &thinsp;&thinsp;&thinsp;&thinsp;&thinsp; : &thinsp;&thinsp;&thinsp;&thinsp;&thinsp; <?= ucwords($data->jurusan) ?> </td>
        				</tr>
                    </table>
                    <table border="1" width="100%" style="border-collapse:collapse">
                        <tr>
                            <th style="font-size:12px;">NO</th>
                            <th style="font-size:12px;">Mata Kuliah</th>
                            <th style="font-size:12px;">SKS</th>
                            <th style="font-size:12px;">Dosen Pengampu</th>
                            <th style="font-size:12px;">Ttd. Pengawas</th>
                        </tr>
                        <?php $no = 1; foreach($makul as $m): ?>
                        <tr>
                            <td style="font-size:12px; padding-left:5px;"><?= $no++ ?></td>
                            <td style="font-size:12px; padding-left:5px;"><?= $m['nama_makul'] ?></td>
                            <td style="font-size:12px; padding-left:5px;"><?= $m['sks'] ?></td>
                            <td style="font-size:12px; padding-left:5px;"><?= ucwords($m['nama_dosen']) ?></td>
                            <td style="font-size:12px; padding-left:5px;"></td>
                        </tr>
                        <?php endforeach ?>
                    </table>

                    <table width="100%" style="border-collapse:collapse; margin-top:10px">
                        <tr>
                            <td style="width: 75%;font-size:12px; font-weight:bold;">Kartu Wajib dibawa selama UJIAN <br> Berlaku s/d Selesai</td>
                            <td style="width: 25%;font-size:12px; font-weight:bold;">Kendal, <?= date('d F Y') ?> <br> Ketua Panitia <br> <br> <br> <br> <br> Hasnan Afif, M.Kom.</td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>

    </body>
</html>
