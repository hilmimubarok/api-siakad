<?php

namespace App\Controllers;

use CodeIgniter\RESTful\ResourceController;
use CodeIgniter\API\ResponseTrait;
use App\Models\NilaiModel;


class Nilai extends ResourceController
{
	use ResponseTrait;

	public $model;

	public function __construct()
	{
		return $this->model = new NilaiModel();
	}

	public function get($id_dosen)
	{
		$data = $this->model->join('mahasiswa', 'mahasiswa.id_mahasiswa = nilai.mahasiswa_id')->join('mata_kuliah', 'mata_kuliah.id_makul = nilai.makul_id')->join('dosen', 'mata_kuliah.dosen_id = '.$id_dosen)->getWhere(['dosen.id_dosen' => $id_dosen])->getResultArray();

		return $this->respond($data, 200);
	}

	public function index()
	{
		$data = $this->model->join('mahasiswa', 'mahasiswa.id_mahasiswa = nilai.mahasiswa_id', 'left')->join('mata_kuliah', 'mata_kuliah.id_makul = nilai.makul_id')->findAll();

		return $this->respond($data, 200);
	}

	public function show($id = null)
	{
		$data = $this->model->join('mahasiswa', 'mahasiswa.id_mahasiswa = nilai.mahasiswa_id')->getWhere([
			'id_nilai' => $id
		])->getResult();

		if ($data) {
			return $this->respond($data, 200);
		} else {
			return $this->failNotFound("Data dengan ID $id tidak ditemukan");
		}
	}

	public function create()
	{
		$data = [
			'mahasiswa_id' => $this->request->getPost('mahasiswa_id'),
			'makul_id'     => $this->request->getPost('makul_id'),
			'absen'        => $this->request->getPost('absen'),
			'keaktifan'    => $this->request->getPost('keaktifan'),
			'tugas'        => $this->request->getPost('tugas'),
			'uts'          => $this->request->getPost('uts'),
			'uas'          => $this->request->getPost('uas'),
			'skor'         => $this->request->getPost('skor'),
			'huruf'        => $this->request->getPost('huruf'),
			'bobot'        => $this->request->getPost('bobot'),
			'keterangan'   => $this->request->getPost('keterangan')
		];

		$this->model->insert($data);

		$response = [
			'status'   => 200,
			'error'    => null,
			'id_nilai' => $this->model->insertID,
			'messages' => [
				'success' => 'Data Berhasil Disimpan'
			]
		];

		return $this->respondCreated($response, 200);
	}

	public function update($id = null)
	{
		return $id;
	}

	public function delete($id = null)
	{
		$data = $this->model->where('id_nilai', $id)->find();

		if ($data) {
			$this->model->where('id_nilai', $id)->delete();
			$response = [
				'status'   => 200,
				'error'    => null,
				'messages' => [
					'success' => 'Data Berhasil Dihapus'
				]
			];
			return $this->respondDeleted($response);
		} else {
			return $this->failNotFound("Data dengan ID $id tidak ditemukan");
		}
	}

}
