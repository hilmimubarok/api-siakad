<?php

namespace App\Controllers;

use CodeIgniter\RESTful\ResourceController;
use CodeIgniter\API\ResponseTrait;
use App\Models\MakulModel;
use App\Models\MahasiswaModel;


class Makul extends ResourceController
{
	use ResponseTrait;

	public $model;
	public $mhs;

	public function __construct()
	{
		return $this->model = new MakulModel();
	}

	public function index()
	{
		$data = $this->model->join('dosen', 'mata_kuliah.dosen_id = dosen.id_dosen', 'left')->findAll();

		return $this->respond($data, 200);
	}

	public function show($id = null, $field = "id_makul")
	{
		$data = $this->model->join('dosen', 'mata_kuliah.dosen_id = dosen.id_dosen', 'left')->getWhere([
			"mata_kuliah.".$field => $id
		])->getResult();

		if ($data) {
			return $this->respond($data, 200);
		} else {
			$response = [
				'status'   => 201,
				'error'    => null,
				'messages' => [
					'success' => 'Data Tidak Ditemukan'
				]
			];
			return $this->respond($response, 201);
		}
	}

	public function create()
	{
		$data = [
			'id_makul'   => $this->request->getPost('id_makul'),
			'dosen_id'   => $this->request->getPost('dosen_id'),
			'nama_makul' => $this->request->getPost('nama_makul'),
			'sks'        => $this->request->getPost('sks'),
			'semester'   => $this->request->getPost('semester'),
			'jurusan'    => $this->request->getPost('jurusan')
		];

		$this->model->insert($data);

		$response = [
			'status'   => 200,
			'error'    => null,
			'id_makul' => $this->model->insertID,
			'messages' => [
				'success' => 'Data Berhasil Disimpan'
			]
		];

		return $this->respondCreated($response, 200);
	}

	public function update($id = null)
	{
		$json = $this->request->getJSON();

		if ($json) {
			$data = [
				'dosen_id'   => $json->dosen_id,
				'nama_makul' => $json->nama_makul,
				'sks'        => $json->sks,
				'semester'   => $json->semester
			];
		} else {
			$input = $this->request->getRawInput();

			$data = [
				'dosen_id'   => $input['dosen_id'],
				'nama_makul' => $input['nama_makul'],
				'sks'        => $input['sks'],
				'semester'   => $input['semester']
			];
		}
		$this->model->update($id, $data);
		$response = [
			'status'   => 200,
			'error'    => null,
			'messages' => [
				'success' => 'Data Berhasil Diupdate'
			]
		];

		return $this->respond($response);
	}

	public function delete($id = null, $field = "id_makul")
	{
		$data = $this->model->where($field, $id)->find();

		if ($data) {
			$this->model->where($field, $id)->delete();
			$response = [
				'status'   => 200,
				'error'    => null,
				'messages' => [
					'success' => 'Data Berhasil Dihapus'
				]
			];
			return $this->respondDeleted($response);
		} else {
			return $this->failNotFound("Data dengan ID $id tidak ditemukan");
		}
	}

	public function kurikulum($jurusan)
	{
		$data = $this->model->orderBy('semester', 'ASC')->join('dosen', 'dosen.id_dosen = mata_kuliah.dosen_id')->join('jadwal', 'jadwal.makul_id = mata_kuliah.id_makul')->getWhere([
			'jurusan' => $jurusan
			])->getResult();

		return $this->respond($data);
	}

	public function sks($id, $semester, $jurusan, $kelas)
	{
		$data = $this->model->join('dosen', 'dosen.id_dosen = mata_kuliah.dosen_id')->join('jadwal', 'mata_kuliah.id_makul = jadwal.makul_id')->getWhere([
			'mata_kuliah.semester' => $semester,
			'mata_kuliah.jurusan'  => $jurusan,
			'jadwal.kelas'         => $kelas
			])->getResult();

		$this->setTemp($id, $kelas, $semester);

		return $this->respond($data);
	}

	public function setTemp($id, $kelas, $semester)
	{
		$this->mhs = new MahasiswaModel();

		$data = [
			'temp_kelas'    => $kelas,
			'temp_semester' => $semester
		];

		$this->mhs->update($id, $data);
	}

}
