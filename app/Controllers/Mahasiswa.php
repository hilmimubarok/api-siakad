<?php

namespace App\Controllers;

use CodeIgniter\RESTful\ResourceController;
use CodeIgniter\API\ResponseTrait;
use App\Models\MahasiswaModel;
use App\Models\MakulModel;
use App\Models\NilaiModel;
use Mpdf\Mpdf;


class Mahasiswa extends ResourceController
{
	use ResponseTrait;

	public $model;
	public $makul;
	public $nilai;

	public function __construct()
	{

		return $this->model = new MahasiswaModel();
	}

	public function index()
	{
		$data = $this->model->findAll();
		// $rows = [];
		// foreach($data as $row) {
		// 	$row['foto'] = "https://apisiakad.hilmimubarok.com/assets/img/profile/" .$row['foto'];
		// 	$rows[]      = $row;
		// }

		return $this->respond($data, 200);
	}

	public function getBySemester($jurusan, $semester)
	{
		$data = $this->model->getWhere(['jurusan' => $jurusan, 'semester' => $semester])->getResultArray();

		return $this->respond($data, 200);

	}

	public function show($id = null)
	{
		$data = $this->model->getWhere([
			'id_mahasiswa' => $id
		])->getResult();

		if ($data) {
			return $this->respond($data, 200);
		} else {
			return $this->failNotFound("Data dengan ID $id tidak ditemukan");
		}
	}

	public function create()
	{
		$data = [
			'user_id'       => $this->request->getPost('user_id'),
			'nim'           => $this->request->getPost('nim'),
			'nama'          => $this->request->getPost('nama'),
			'foto'          => $this->request->getPost('foto'),
			'email'         => $this->request->getPost('email'),
			'jenis_kelamin' => $this->request->getPost('jenis_kelamin'),
			'tahun_masuk'   => $this->request->getPost('tahun_masuk'),
			'kelas'         => $this->request->getPost('kelas'),
			'temp_kelas'    => $this->request->getPost('kelas'),
			'jurusan'       => $this->request->getPost('jurusan'),
			'semester'      => $this->request->getPost('semester'),
			'temp_semester' => $this->request->getPost('semester'),
			'status_khs'    => $this->request->getPost('status_khs'),
			'status_krs'    => $this->request->getPost('status_krs'),
			'status_uts'    => $this->request->getPost('status_uts'),
			'status_uas'    => $this->request->getPost('status_uas')
		];

		$this->model->insert($data);

		$response = [
			'status'   => 200,
			'error'    => null,
			'messages' => [
				'success' => 'Data Berhasil Disimpan'
			]
		];

		return $this->respondCreated($response, 200);
	}

	public function update($id = null)
	{
		$json = $this->request->getJSON();

		if ($json) {
			$data = [
				'kelas'    => $json->kelas,
				'semester' => $json->semester
			];
		} else {
			$input = $this->request->getRawInput();

			$data = [
				'kelas'    => $input['kelas'],
				'semester' => $input['semester']
			];
		}
		$this->model->update($id, $data);
		$response = [
			'status'   => 200,
			'error'    => null,
			'messages' => [
				'success' => 'Data Berhasil Diupdate'
			]
		];

		return $this->respond($response);
	}

	public function delete($id = null)
	{
		$data = $this->model->where('user_id', $id)->find();

		if ($data) {
			$this->model->where('user_id', $id)->delete();
			$response = [
				'status'   => 200,
				'error'    => null,
				'messages' => [
					'success' => 'Data Berhasil Dihapus'
				]
			];
			return $this->respondDeleted($response);
		} else {
			return $this->failNotFound("Data dengan ID $id tidak ditemukan");
		}
	}

	public function import()
	{
		$data      = json_decode($this->request->getBody(),true);

		$this->model->insertBatch($data);

		$response = [
			'status'   => 200,
			'error'    => null,
			'data'     => $data,
			'messages' => [
				'success' => 'Data Berhasil Diimport'
			]
		];
		return $this->respond($response);
	}

	public function getMakul($semester = null, $jurusan = null)
	{
		$this->makul = new MakulModel();

		$data = $this->makul->join('dosen', 'mata_kuliah.dosen_id = dosen.id_dosen', 'left')->getWhere(['semester' => $semester, 'jurusan' => $jurusan])->getResult();

		return $this->respond($data);
	}

	public function getSemester($id = null)
	{
		$data = $this->model->getWhere([
			'user_id' => $id
			])->getResultArray();

		$rows = [];

		foreach($data as $row) {
			$row['semester'] = $this->_getSemester($row['tahun_masuk']);
			$rows[]          = $row;
		}

		$rows = array_values($rows);

		if ($rows) {
			return $this->respond($rows, 200);
		} else {
			return $this->failNotFound("Data dengan ID $id tidak ditemukan");
		}
	}

	public function transkipNilai($mahasiswa_id)
	{
		$this->nilai = new NilaiModel();
		$data        = $this->nilai->join('mata_kuliah', 'mata_kuliah.id_makul = nilai.makul_id')->getWhere(
			['mahasiswa_id' => $mahasiswa_id]
		)->getResult();

		return $this->respond($data);
	}

	private function _getSemester($tahun_masuk)
	{
		$month = date("m");
		$now   = date("Y");

		if ($month < 2 || $month > 8) {
			// Genap
			return ($now - $tahun_masuk) * 2 + 1;
		} else {
			// Ganjil
			return ($now - $tahun_masuk) * 2;
		}
	}

	public function setStatus($id)
	{
		$json = $this->request->getJSON();

		if ($json) {
			$data = [
				'status_khs' => $json->status_khs,
				'status_krs' => $json->status_krs,
				'status_uts' => $json->status_uts,
				'status_uas' => $json->status_uas
			];
		} else {
			$input = $this->request->getRawInput();

			$data = [
				'status_khs' => $input['status_khs'],
				'status_krs' => $input['status_krs'],
				'status_uts' => $input['status_uts'],
				'status_uas' => $input['status_uas']
			];
		}

		$this->model->update($id, $data);
		$response = [
			'status'   => 200,
			'error'    => null,
			'messages' => [
				'success' => 'Data Berhasil Diupdate'
			]
		];

		return $this->respond($response);
	}

	public function changePhoto($id = null)
	{
		$file = $this->request->getFile('file');

		$name = $file->getName();
		$file->move('./assets/img/profile', $name);

		$data = [
			'foto' => "http://api.siakad-filkomuniss.com/assets/img/profile/".$name
		];

		$this->model->update($id, $data);

		$response = [
			'status'   => 200,
			'error'    => null,
			'messages' => [
				'link'    => "http://api.siakad-filkomuniss.com/assets/img/profile/" .$name,
				'success' => 'Data Berhasil Diupdate'
			]
		];

		return $this->respond($response);
	}

	public function closeAll($status)
	{

		switch ($status) {
			case 'krs':
				$this->setTempMhs();
				$data = ['status_krs' => 'tutup'];
				break;
			case 'khs':
				$data = ['status_khs' => 'tutup'];
				break;
			case 'uts':
				$data = ['status_uts' => 'tutup'];
				break;
			case 'uas':
				$data = ['status_uas' => 'tutup'];
				break;
		}

		$this->model->update(null, $data);
		$response = [
			'status'   => 200,
			'error'    => null,
			'messages' => [
				'success' => 'Data Berhasil Diupdate'
			]
		];

		return $this->respond($response);
	}

	public function setTempMhs()
	{
		$db      = \Config\Database::connect();
		$builder = $db->table('mahasiswa');
		$mhs     = $db->query("SELECT * FROM mahasiswa WHERE temp_kelas != kelas OR temp_semester != semester ")->getResultArray();

		$data = [];
		foreach ($mhs as $m) {
			$m['kelas']    = $m['temp_kelas'];
			$m['semester'] = $m['temp_semester'];
			$data[]        = $m;
		}

		if (!empty($data))
			$builder->updateBatch($data, 'id_mahasiswa');

		// return null;

	}

	public function checkStatus($id_mahasiswa)
	{
		$data = $this->model->where(['id_mahasiswa' => $id_mahasiswa])->find();

		if (!$data)
			return $this->failNotFound("data dengan id {$id_mahasiswa} tidak ditemukan");

		unset($data[0]['id_mahasiswa']);
		unset($data[0]['user_id']);
		unset($data[0]['nim']);
		unset($data[0]['nama']);
		unset($data[0]['foto']);
		unset($data[0]['email']);
		unset($data[0]['jenis_kelamin']);
		unset($data[0]['tahun_masuk']);
		unset($data[0]['jurusan']);
		unset($data[0]['kelas']);
		unset($data[0]['temp_kelas']);
		unset($data[0]['semester']);
		unset($data[0]['temp_semester']);

		return $this->respond($data);
	}

	public function print($id_mahasiswa, $status, $jurusan)
	{
		$this->makul   = new MakulModel();
		$data['data']  = $this->model->getWhere(['id_mahasiswa' => $id_mahasiswa, 'jurusan' => $jurusan])->getRow();
		if (!$data['data'])
			return $this->failNotFound("data dengan id {$id_mahasiswa} tidak ditemukan");

		$data['makul'] = $this->makul->join('dosen', 'mata_kuliah.dosen_id = dosen.id_dosen')->getWhere(['semester' => $data['data']->semester, 'mata_kuliah.jurusan' => $data['data']->jurusan])->getResultArray();
		$data['image'] = base_url(). "/assets/img/uniss.png";

		$mpdf = new Mpdf();

		switch ($status) {
			case 'uts':
				$mpdf->WriteHTML(view('cetak/uts', $data));
				$mpdf->Output();
				break;
			case 'uas':
				$mpdf->WriteHTML(view('cetak/uas', $data));
				$mpdf->Output();
				break;
		}
	}
}
