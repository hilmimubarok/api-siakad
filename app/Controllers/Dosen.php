<?php

namespace App\Controllers;

use CodeIgniter\RESTful\ResourceController;
use CodeIgniter\API\ResponseTrait;
use App\Models\DosenModel;

class Dosen extends ResourceController
{
	use ResponseTrait;

	public $model;

	public function __construct()
	{
		return $this->model = new DosenModel();
	}

	public function index()
	{
		$data = $this->model->findAll();

		// $rows = [];
		// foreach($data as $row) {
		// 	$row['foto'] = "https://apisiakad.hilmimubarok.com/assets/img/profile/" .$row['foto'];
		// 	$rows[]      = $row;
		// }

		return $this->respond($data, 200);
	}

	public function show($id = null, $field = "id_dosen")
	{
		$data = $this->model->getWhere([
			$field => $id
		])->getResult();

		if ($data) {
			return $this->respond($data, 200);
		} else {
			return $this->failNotFound("Data dengan ID $id tidak ditemukan");
		}
	}

	public function create()
	{
		$data = [
			'user_id'             => $this->request->getPost('user_id'),
			'niy'                 => $this->request->getPost('niy'),
			'nama_dosen'          => $this->request->getPost('nama_dosen'),
			'email_dosen'         => $this->request->getPost('email_dosen'),
			'no_hp_dosen'         => $this->request->getPost('no_hp_dosen'),
			'foto_dosen'          => $this->request->getPost('foto_dosen'),
			'jenis_kelamin_dosen' => $this->request->getPost('jenis_kelamin_dosen'),
			'alamat_dosen'        => $this->request->getPost('alamat_dosen'),
			'status_dosen'        => $this->request->getPost('status_dosen'),
			'prodi_dosen'         => $this->request->getPost('prodi_dosen')
		];

		$this->model->insert($data);

		$response = [
			'status'   => 200,
			'error'    => null,
			'messages' => [
				'success' => 'Data Berhasil Disimpan'
			]
		];

		return $this->respondCreated($response, 200);
	}

	public function update($id = null)
	{
		$json = $this->request->getJSON();

		if ($json) {
			$data = [
				'niy'          => $json->niy,
				'nama_dosen'   => $json->nama_dosen,
				'email_dosen'  => $json->email_dosen,
				'no_hp_dosen'  => $json->no_hp_dosen,
				'alamat_dosen' => $json->alamat_dosen
				// 'status_dosen' => $json->status_dosen
			];
		} else {
			$input = $this->request->getRawInput();

			$data = [
				'niy'          => $input['niy'],
				'nama_dosen'   => $input['nama_dosen'],
				'email_dosen'  => $input['email_dosen'],
				'no_hp_dosen'  => $input['no_hp_dosen'],
				'alamat_dosen' => $input['alamat_dosen']
				// 'status_dosen' => $input['status_dosen']
			];
		}
		$this->model->update($id, $data);
		$response = [
			'status'   => 200,
			'error'    => null,
			'messages' => [
				'success' => 'Data Berhasil Diupdate'
			]
		];

		return $this->respond($response);
	}

	public function delete($id = null)
	{
		$data = $this->model->where('user_id', $id)->find();

		if ($data) {
			$this->model->where('user_id', $id)->delete();
			$response = [
				'status'   => 200,
				'error'    => null,
				'messages' => [
					'success' => 'Data Berhasil Dihapus'
				]
			];
			return $this->respondDeleted($response);
		} else {
			return $this->failNotFound("Data dengan ID $id tidak ditemukan");
		}
	}

	public function changePhoto($id = null)
	{
		$file = $this->request->getFile('file');

		$name = $file->getName();
		$file->move('./assets/img/profile', $name);

		$data = [
			'foto_dosen' => "http://api.siakad-filkomuniss.com/assets/img/profile/".$name
		];

		$this->model->update($id, $data);

		$response = [
			'status'   => 200,
			'error'    => null,
			'messages' => [
				'link'    => "http://api.siakad-filkomuniss.com/assets/img/profile/" .$name,
				'success' => 'Data Berhasil Diupdate'
			]
		];

		return $this->respond($response);
	}

}
