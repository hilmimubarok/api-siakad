<?php

namespace App\Controllers;

use App\Libraries\CodeIgniterCORS\CodeIgniterCORS;

/**
 * Class BaseController
 *
 * BaseController provides a convenient place for loading components
 * and performing functions that are needed by all your controllers.
 * Extend this class in any new controllers:
 *     class Home extends BaseController
 *
 * For security be sure to declare any new methods as protected or private.
 *
 * @package CodeIgniter
 */

use CodeIgniter\Controller;

class BaseController extends Controller
{

	/**
	 * An array of helpers to be loaded automatically upon
	 * class instantiation. These helpers will be available
	 * to all other controllers that extend BaseController.
	 *
	 * @var array
	 */
	protected $helpers = [];

	/**
	 * Constructor.
	 */
	public function initController(\CodeIgniter\HTTP\RequestInterface $request, \CodeIgniter\HTTP\ResponseInterface $response, \Psr\Log\LoggerInterface $logger)
	{
		// Do Not Edit This Line
		parent::initController($request, $response, $logger);

		//--------------------------------------------------------------------
		// Preload any models, libraries, etc, here.
		//--------------------------------------------------------------------
		// E.g.:
		// $this->session = \Config\Services::session();

		// ---

		// Add the lines below and both methods "cors()" and "_cors()"

		if (!is_cli())
		{
			$this->_cors();
		}
	}

	public function options(): Response
    {
		// return $this->response->setHeader('Access-Control-Allow-Origin', '*')->setHeader('Access-Control-Allow-Headers', '*')->setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE')->setStatusCode(200);
		// header('Access-Control-Allow-Origin: *');
		// header("Access-Control-Allow-Headers: X-API-KEY, Authorization, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
		// header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");

    }


	/**
	 *
	 */
	public function cors(): void
	{
		// return $this->response->setHeader('Access-Control-Allow-Origin', '*')->setHeader('Access-Control-Allow-Headers', '*')->setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE')->setStatusCode(200);
		header('Access-Control-Allow-Origin: *');
		header("Access-Control-Allow-Headers: X-API-KEY, Authorization, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
		header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
		/**
		 * This method is only needed for the route "$routes->options('(:any)', 'BaseController::cors')".
		 * As the route targets here, the private method "_cors()" will already be called from "initController()".
		 */

	}

	/**
	 *
	 */
	private function _cors(): void
	{
		$ciCORS = new CodeIgniterCORS();
		$ciCORS->handle($this->request, $this->response);
	}

}
