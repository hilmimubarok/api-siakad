<?php

namespace App\Controllers;

use CodeIgniter\RESTful\ResourceController;
use CodeIgniter\API\ResponseTrait;
use CodeIgniter\HTTP\Files\UploadedFile;
use App\Models\MateriModel;


class Materi extends ResourceController
{
	use ResponseTrait;

	public $model;

	public function __construct()
	{
		return $this->model = new MateriModel();
	}

	public function index()
	{
		$data = $this->model->join('mata_kuliah', 'mata_kuliah.id_makul = materi.makul_id', 'left')->findAll();

		return $this->respond($data, 200);
	}

	public function show($id = null, $field = "id_materi")
	{
		$data = $this->model->join('mata_kuliah', 'mata_kuliah.id_makul = materi.makul_id')->getWhere(['materi.'.$field => $id])->getResult();

		if ($data) {
			return $this->respond($data, 200);
		} else {
			return [];
		}
	}

	public function create()
	{
		$file = $this->request->getFile('file');

		if (!$file->isValid())
			return $this->fail($file->getErrorString());

		$data = [
			'makul_id' => $this->request->getPost('makul_id'),
			'judul'    => $this->request->getPost('judul'),
			'file'     => url_title($this->request->getPost('judul'), '-', TRUE). ".". $file->guessExtension(),
			'tanggal'  => $this->request->getPost('tanggal')
		];
		$file->move('./assets/img/materi', url_title($data['judul'], '-', TRUE). ".". $file->guessExtension());

		$this->model->insert($data);

		$response = [
			'status'    => 200,
			'error'     => null,
			'id_materi' => $this->model->insertID,
			'messages'  => [
				'success' => 'Data Berhasil Disimpan'
			]
		];

		return $this->respondCreated($response, 200);
	}

	public function update($id = null)
	{
		helper(['array']);
		$fileName = dot_array_search('file.name', $_FILES);

		// $json = json_decode($this->request->getBody('judul'), TRUE);
		$json = $this->request->getBody('judul');

		var_dump($json); die;

		if ($json) {
			$data = [
				'judul'    => $json->judul,
				'file'     => $fileName->getName()
			];
		} else {
			$input = $this->request->getRawInput();
			$data = [
				'judul'    => $input['judul'],
				'file'     => $fileName->getName()
			];
		}
		$file->move('./assets/img/materi', url_title($data['judul'], '-', TRUE). ".". $file->guessExtension());
		$this->model->update($id, $data);
		$response = [
			'status'   => 200,
			'error'    => null,
			'messages' => [
				'success' => 'Data Berhasil Diupdate'
			]
		];

		return $this->respond($response);
	}

	public function delete($id = null)
	{
		$data = $this->model->where('id_materi', $id)->find();

		$file = "./assets/img/materi/". $data[0]['file'];

		if ($data) {
			if (file_exists($file)) {
				unlink($file);
			}
			$this->model->where('id_materi', $id)->delete();
			$response = [
				'status'   => 200,
				'error'    => null,
				'messages' => [
					'success' => 'Data Berhasil Dihapus'
				]
			];
			return $this->respondDeleted($response);
		} else {
			return $this->failNotFound("Data dengan ID $id tidak ditemukan");
		}
	}

	public function loadFile($id)
	{
		$path = "./assets/img/materi";
		$data = $this->model->join('mata_kuliah', 'mata_kuliah.id_makul = materi.makul_id')->getWhere([
			'id_materi' => $id
		])->getResult();

		if ($data) {
			if (file_exists($path."/".$data[0]->file)) {
				readfile($path."/".$data[0]->file);
			}
		} else {
			$response = [
				'status'   => 201,
				'error'    => TRUE,
				'messages' => [
					'error' => 'Data Tidak Ada'
				]
			];
			return $this->respond($response, 201);
		}

	}

}
