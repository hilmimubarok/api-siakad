<?php

namespace App\Controllers;

use CodeIgniter\RESTful\ResourceController;
use CodeIgniter\API\ResponseTrait;
use App\Models\UserModel;


class Users extends ResourceController
{

	use ResponseTrait;

	public $model;

	public function __construct()
	{
		// load model UserModel
		return $this->model = new UserModel();
	}

	public function index()
	{
		$data = $this->model->findAll();

		return $this->respond($data, 200);
	}

	public function show($id = null)
	{
		$data = $this->model->getWhere([
			'id_user' => $id
		])->getResult();

		if ($data) {
			return $this->respond($data, 200);
		} else {
			return $this->failNotFound("Data dengan ID $id tidak ditemukan");
		}
	}

	public function create()
	{
		$data = [
			'username' => $this->request->getPost('username'),
			'password' => password_hash($this->request->getPost('password'), PASSWORD_BCRYPT),
			'role'     => $this->request->getPost('role')
		];

		$this->model->insert($data);

		$response = [
			'status'   => 200,
			'error'    => null,
			'user_id'  => $this->model->insertID,
			'messages' => [
				'success' => 'Data Berhasil Disimpan'
			]
		];

		return $this->respondCreated($response, 200);
	}

	public function update($id = null)
	{
		$json = $this->request->getJSON();

		if ($json) {
			$data = [
				'username' => $json->username,
				'password' => $json->password
			];
		} else {
			$input = $this->request->getRawInput();

			$data = [
				'username' => $input['username'],
				'password' => $input['password']
			];
		}
		// Update data
		$this->model->update($id, $data);
		$response = [
			'status'   => 200,
			'error'    => null,
			'messages' => [
				'success' => 'Data Berhasil Diupdate'
			]
		];

		return $this->respond($response);
	}

	public function delete($id = null)
	{
		$data = $this->model->find($id);

		if ($data) {
			$this->model->delete($id);
			$response = [
				'status'   => 200,
				'error'    => null,
				'messages' => [
					'success' => 'Data Berhasil Dihapus'
				]
			];
			return $this->respondDeleted($response);
		} else {
			return $this->failNotFound("Data dengan ID $id tidak ditemukan");
		}
	}

	public function import()
	{
		$data      = json_decode($this->request->getBody(),true);
		$user_id   = [];
		$data_user = [];

		foreach ($data as $user) {
			$data_user[] = [
				'username' => $user['nim'],
				'password' => password_hash($user['nim'], PASSWORD_BCRYPT),
				'role' => 'mahasiswa'
			];
		}

		foreach ($data_user as $datauser) {
			$this->model->insert($datauser);
			$user_id[] = [
				'user_id' => $this->model->insertID
			];
		}

		$response = [
			'status'   => 200,
			'error'    => null,
			'user_id'  => $user_id,
			'messages' => [
				'success' => 'Data Berhasil Diimport'
			]
		];
		return $this->respond($response);
	}

}

?>
