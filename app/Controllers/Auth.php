<?php

namespace App\Controllers;

use \Firebase\JWT\JWT;
use App\Models\AuthModel;
use App\Models\MahasiswaModel;
use App\Models\DosenModel;
use App\Models\UserModel;
use App\Models\MakulModel;
use CodeIgniter\RESTful\ResourceController;
use CodeIgniter\API\ResponseTrait;

class Auth extends ResourceController
{

	use ResponseTrait;

	public $model, $mhs, $dosen, $role, $user, $makul;
	public $profile = null;
    private $expire_at;


    public function __construct()
    {
		$this->expire_at = time() - 60;
        return $this->model = new AuthModel();
    }

    public function index()
    {
        return view('welcome');
    }

    private function privateKey()
    {
        $privateKey = <<<EOD
            -----BEGIN RSA PRIVATE KEY-----
            MIICXAIBAAKBgQCWVSPF/cT63pA8flKiSrzTGOfP8OcBlxSiaszkD1Zeb1D/lBi2
            86e9n8Pt/hGEwpLrjjc+uOrYOIav0Xt4cK0YuoeK14aYzegGmTnTcb3I1yIJrX9W
            4b70sgIZs8DBMyFMTXYCWv75X7OazXBMn3GLSzmng0jCrqfVLq29yrQolwIDAQAB
            AoGAclGqU4zIN3H6Q1ZYpnupHeppldJ/CNGH6n7G8oLLTiLeeIx5Qt8E5wlQaHH1
            hnykVyad0j+yrtxFZ/woJY2hwuFG/PKqcpCV91T9xnZo7GndlnRiCOSBUXc4/n3I
            UzIbmBGisTpSdZ8qzRai2rbdfrqFM6WSGnScS21rcs/o0ekCQQD82v2Y+aeTSczv
            pKDt6IZc7zgmtDz132K3jgXyU5MHWRCIn6pbhPUqoSBprnpfes0PR9GvUGOtizGj
            JuL2XFN7AkEAmDO/7i3S8UseRgWXlap0wcN+RoPGsBsv/KuONNrxRH2xeJqj2io3
            pgrehz324FT74qM3bWakOTTXzshnU/EWlQJBAPDnF2vyl2RtkLHU0Ho56iGKzJ2b
            ZOUAAkZNKn84fsEmaukv7ZeqMgZ+YqotUGu9TLc9Pppf5cfG8PC3MM91IRcCQAk5
            ebRxyosdRmRwClPj1Ne5hXm4mt4ph6sEqsXBT0YbaBorJSsfy+egWQRRWRkXjXya
            780Nn35oGXxK4RhRUFECQBtMHHST6YOSJbLvxDl7DJ48dMJUmh+j+JgmEbhFMJLa
            sG/5Jm+d2jKUzD3/eMBmIYuAVg+OUik0XqxjIQW/acY=
            -----END RSA PRIVATE KEY-----
            EOD;
        return $privateKey;
    }

    private function generateToken(array $token)
    {
        $secret_key = $this->privateKey();
        return JWT::encode($token, $secret_key);
    }

    public function login()
    {
		$username  = $this->request->getPost('username');
		$password  = $this->request->getPost('password');

		$cek_login = $this->model->getWhere([
			'username' => $username
		])->getResult();

		// Cek Role User Login
		if ($cek_login[0]->role == "mahasiswa") {
			$this->mhs = new MahasiswaModel();
			$data      = $this->mhs->getWhere([
				'user_id' => $cek_login[0]->id_user
			])->getResultArray();
			// $rows      = [];
			// foreach($data as $row) {
			// 	$row['foto']     = "https://apisiakad.hilmimubarok.com/assets/img/profile/" .$row['foto'];
			// 	$rows[]          = $row;
			// }
			$this->profile = $data;
		} elseif ($cek_login[0]->role == "dosen") {
			$this->dosen = new DosenModel();
			$this->makul = new MakulModel();
			$data        = $this->dosen->getWhere(['user_id' => $cek_login[0]->id_user])->getResultArray();
			// $rows        = [];
			// foreach($data as $row) {
			// 	$row['foto_dosen']     = "https://apisiakad.hilmimubarok.com/assets/img/profile/" .$row['foto_dosen'];
			// 	$rows[]          = $row;
			// }
			$this->profile = $data;

			array_push($this->profile, [
				"makul" => $this->makul->getWhere(['dosen_id' => $this->profile[0]['id_dosen']])->getResult()
			]);

		}


        if(password_verify($password, $cek_login[0]->password))
        {
            $data = [
                "iss"  => "THE_CLAIM",
                "aud"  => "THE_AUDIENCE",
                "iat"  => time(),
                // "nbf"  => time() + 10,
                "exp"  => time() + $this->expire_at,
                "data" => [
                    "id"       => $cek_login[0]->id_user,
                    "username" => $cek_login[0]->username,
                    "role"     => $cek_login[0]->role
                ]
            ];

            $token  = $this->generateToken($data);

            $output = [
				"status"   => 200,
				"message"  => 'Berhasil login',
				"token"    => $token,
				"username" => $data['data']['username'],
				"expireAt" => $data['exp'],
				"profile"  => $this->profile
            ];

            return $this->respond($output, 200);

        } else {
            $output = [
				'status'   => 201,
				'message'  => 'Login failed',
				"password" => $password
            ];

            return $this->respond($output, 201);
        }
    }

    public function refreshToken()
    {

        $id       = $this->request->getPost('id');
        $username = $this->request->getPost('username');
        $role     = $this->request->getPost('role');

        $data     = [
            "iss"  => "THE_CLAIM",
            "aud"  => "THE_AUDIENCE",
            "iat"  => time(),
            // "nbf"  => time() + 10,
            "exp"  => time() + $this->expire_at,
            "data" => [
                "id"       => $id,
                "username" => $username,
                "role"     => $role
            ]
        ];

        $token  = $this->generateToken($data);

        $output = [
            'status'   => 200,
            'message'  => 'Berhasil refresh token',
            "token"    => $token,
            "username" => $data['data']['username'],
            "expireAt" => $data['exp']
        ];

        return $this->respond($output, 200);

    }

	public function refreshTemp()
	{
		$this->mhs = new MahasiswaModel();
		$get_mhs   = $this->mhs->getWhere([
			'temp_kelas' => "",
			'temp_semester' => 0
		])->getResultArray();
		var_dump($get_mhs);
		$data = [];
		foreach ($get_mhs as $m) {
			$m['temp_kelas']    = $m['kelas'];
			$m['temp_semester'] = $m['semester'];
			$data[]             = $m;
		}
		$db      = \Config\Database::connect();
		$builder = $db->table('mahasiswa');
		if (!empty($data))
			$builder->updateBatch($data, 'id_mahasiswa');
	}

    public function changePassword($id = null)
    {
        $json = $this->request->getJSON();
        if ($json) {
            $data = [
                'username' => $json->username,
                'password' => $json->password
            ];
        } else {
            $input = $this->request->getRawInput();
            $data  = [
                'username' => $input['username'],
                'password' => password_hash($input['password'], PASSWORD_BCRYPT)
            ];
            // Update data
            $this->model->update($id, $data);
            $response = [
                'status'   => 200,
                'error'    => null,
                'data' => $data,
                'messages' => [
                    'success' => 'Data Berhasil Diupdate'
                ]
            ];

            return $this->respond($response);
        }
    }

	private function _getSemesterMhs($tahun_masuk)
	{
		$month = date("m");
		$now   = date("Y");

		if ($month < 2 || $month > 8) {
			// Genap
			return ($now - $tahun_masuk) * 2 + 1;
		} else {
			// Ganjil
			return ($now - $tahun_masuk) * 2;
		}
	}

	public function forgotPassword()
	{
		$this->user = new UserModel();
		$email      = $this->request->getPost('email');

		$this->role = new MahasiswaModel();
		$get_role   = $this->role->findByEmail($email);

		if ($get_role == null) {
			$this->role = new DosenModel();
			$get_role = $this->role->findByEmail($email);
		}

		// var_dump($get_role); die;
		//
		// if ($role == "mahasiswa") {
		// 	$this->role = new MahasiswaModel();
		// } else {
		// 	$this->role = new DosenModel();
		// }

		// Get the users by email. Dosen or Mahasiswa
		// $get_role = $this->role->findByEmail($email);

		if ($get_role !== null) {
			$get_user = $this->user->find($get_role);
		} else {
			$get_user = null;
		}

		// Encrypt the data with JWT
		$data = [
            "iss"  => "THE_CLAIM",
            "aud"  => "THE_AUDIENCE",
            "iat"  => time(),
            // "nbf"  => time() + 5,
            "exp"  => time() + 1800,
            "data" => [
                "user" => $get_user
            ]
        ];

        $token  = $this->generateToken($data);

		$msg = '
			<!DOCTYPE html><html lang="en"><head><meta charset="UTF-8"><meta name="viewport" content="width=device-width, initial-scale=1.0"><title>Reset Password</title></head><body style="margin: 0; padding: 0;"><div style="background-color: #3266F5; border-radius: 10px; max-height: 450px; padding: 10px; text-align: center;"><div style="text-align: left; margin-left: 50px;"><p style="color: #f1f1f1; font-size: 38px; padding: 0; font-weight: bold;">Hai '.strstr($email, '@', true).'</p><p style="margin-top: -50px; color: #f1f1f1; font-weight: 300; font-size: 18px">Silahkan reset password anda dengan mengeklik tombol dibawah</p></div><a href="https://siakad-filkomuniss.com/#/pages/resetpassword?token='.$token.'"><button style="padding: 10px; width: 50%; margin-bottom: 20px; margin-top: 20px; border: none; border-radius: 6px; color: #3266F5; background-color: #f1f1f1; font-size: 18px; font-weight: bold;">Reset Password Saya</button></a></div></body></html>
		';

		if ($get_user !== null) {
			return $this->_sendMail($email, $msg, $token);
		} else {
			$response = [
				"message" => "User tidak ditemukan"
			];
			return $this->respond($response);
		}

	}

	private function _sendMail($to, $data, $token)
	{
		$email = \Config\Services::email();

		$email->setFrom('admin@siakad-filkomuniss.com', 'Admin Siakad Filkom');
		$email->setTo($to);

		$email->setSubject('Reset Password #'. mt_rand());
		$email->setMessage($data);

		$email->send($data);

		$response = [
			'message' => 'Link Berhasil dikirim'
		];

		return $this->respond($response, 200);
	}

	public function resetPassword()
	{
		$token        = $this->request->getGet('token');
		$new_password = $this->request->getPost('password');
		$secret_key   = $this->privateKey();

		try {
			// Decode the token
			$decoded       = JWT::decode($token, $secret_key, array('HS256'));
			$id_user       = $decoded->data->user->id_user;

			// UPDATE PASSWORD HERE
			$this->user    = new UserModel();
			$update        = $this->user->update($id_user, [
				'password' => password_hash($new_password, PASSWORD_BCRYPT)
			]);

			$response      = [
				"message"  => "Password berhasil diupdate. Silahkan login kembali"
			];

		} catch (\Exception $e) {
			return $this->respond([
				"message" => $e->getMessage()
			]);
		}

		return $this->respond($response);

	}

	public function refresh()
	{
		$token      = $this->request->getGet('token');
		$secret_key = $this->privateKey();

		try {
			// Decode the token
			$decoded       = JWT::decode($token, $secret_key, array('HS256'));
			// cek Role
			if ($decoded->data->role == "mahasiswa") {
				$this->mhs = new MahasiswaModel();
				$data      = $this->mhs->getWhere([
					'user_id' => $decoded->data->id
				])->getResultArray();
			}

			$response = $data;

		} catch (\Exception $e) {
			return $this->respond([
				"message" => $e->getMessage()
			]);
		}
		return $this->respond($response);
	}

}
