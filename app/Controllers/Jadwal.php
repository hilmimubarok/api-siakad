<?php

namespace App\Controllers;

use CodeIgniter\RESTful\ResourceController;
use CodeIgniter\API\ResponseTrait;
use App\Models\JadwalModel;


class Jadwal extends ResourceController
{
	use ResponseTrait;

	public $model;

	public function __construct()
	{
		return $this->model = new JadwalModel();
	}

	public function index()
	{
		$data = $this->model->join('mata_kuliah', 'mata_kuliah.id_makul = jadwal.makul_id')->findAll();

		return $this->respond($data, 200);
	}

	public function show($id = null)
	{
		$data = $this->model->join('mata_kuliah', 'mata_kuliah.id_makul = jadwal.makul_id')->getWhere([
			'id_jadwal' => $id
		])->getResult();

		if ($data) {
			return $this->respond($data, 200);
		} else {
			return $this->failNotFound("Data dengan ID $id tidak ditemukan");
		}
	}

	public function create()
	{
		$data = [
			'makul_id' => $this->request->getPost('makul_id'),
			'mulai'    => $this->request->getPost('mulai'),
			'selesai'  => $this->request->getPost('selesai'),
			'hari'     => $this->request->getPost('hari'),
			'kelas'     => $this->request->getPost('kelas')
		];

		$this->model->insert($data);

		$response = [
			'status'    => 200,
			'error'     => null,
			'id_jadwal' => $this->model->insertID,
			'messages'  => [
				'success' => 'Data Berhasil Disimpan'
			]
		];

		return $this->respondCreated($response, 200);
	}

	public function update($id = null)
	{
		$json = $this->request->getJSON();

		if ($json) {
			$data = [

				'mulai'    => $json->mulai,
				'selesai'  => $json->selesai,
				'hari'     => $json->hari
			];
		} else {
			$input = $this->request->getRawInput();

			$data = [

				'mulai'    => $input['mulai'],
				'selesai'  => $input['selesai'],
				'hari'     => $input['hari']
			];
		}
		$this->model->update($id, $data);
		$response = [
			'status'   => 200,
			'error'    => null,
			'messages' => [
				'success' => 'Data Berhasil Diupdate'
			]
		];

		return $this->respond($response);
	}

	public function delete($id = null)
	{
		$data = $this->model->where('id_jadwal', $id)->find();

		if ($data) {
			$this->model->where('id_jadwal', $id)->delete();
			$response = [
				'status'   => 200,
				'error'    => null,
				'messages' => [
					'success' => 'Data Berhasil Dihapus'
				]
			];
			return $this->respondDeleted($response);
		} else {
			return $this->failNotFound("Data dengan ID $id tidak ditemukan");
		}
	}

}
