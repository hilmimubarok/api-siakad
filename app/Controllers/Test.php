<?php

namespace App\Controllers;

use CodeIgniter\RESTful\ResourceController;
use CodeIgniter\API\ResponseTrait;
use App\Models\TestModel;
use TCPDF;
use Dompdf\Dompdf;
use Dompdf\Options;
use Mpdf\Mpdf;


class Test extends ResourceController
{
	use ResponseTrait;

	public $model;

	public function __construct()
	{
		return $this->model = new TestModel();
	}

	public function index()
	{
		$data = $this->model->findAll();

		return $this->respond($data, 200);
	}

	public function show($id = null)
	{
		$data = $this->model->getWhere([
			'id_dosen' => $id
		])->getResult();

		if ($data) {
			return $this->respond($data, 200);
		} else {
			return $this->failNotFound("Data dengan ID $id tidak ditemukan");
		}
	}

	public function export()
	{
		$pdf     = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);
		$pdf->SetCellPadding(0);
		$tagvs = array('p' => array(0 => array('h' => 0, 'n' => 0), 1 => array('h' => 0, 'n'
=> 0)));
		$pdf->setHtmlVSpace($tagvs);

		$data =
<<<EOF
<style>
  * {
	  margin:0; padding:0;
  }
  h1, h2 {
    float: left;
    width: 50%;
  }
</style>
<h1>
  Hello World
</h1>
<h2>
  Test
</h2>
EOF;


		$pdf->addPage();
		$pdf->writeHTML($data, true, false, true, false, '');

		$this->response->setContentType('application/pdf');

		return $pdf->Output();
	}

	public function dom()
	{
		$options = new Options();
		$options->set('isRemoteEnabled',true);
		$dompdf = new Dompdf($options);
		$dompdf->set_option('enable_css_float', true);
		$html   = view('dom');
		$dompdf->loadHtml($html);

		$dompdf->setPaper('A4', 'landscape');

    	$dompdf->render('krs');

		return $dompdf->output();
	}

	public function mpd()
	{
		$mpdf = new \Mpdf\Mpdf();
		$mpdf->WriteHTML(view('mpdf'));
		$mpdf->Output();
	}

}
