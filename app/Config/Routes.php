<?php namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php'))
{
	require SYSTEMPATH . 'Config/Routes.php';
}

/**
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Auth');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(false);

/**
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.

$routes->get('refresh', 'Auth::refresh');
$routes->get('refreshtemp', 'Auth::refreshTemp');

// Auth
$routes->post('auth/login', 'Auth::login');
$routes->post('auth/refreshtoken', 'Auth::refreshToken');
$routes->post('auth/changepassword/(:segment)', 'Auth::changePassword/$1');
$routes->post('forgotpassword', 'Auth::forgotPassword');

$routes->group('admin', ['filter' => 'adminfilter'], function ($routes)
{
	// CRUD Mahasiswa
	$routes->post('mahasiswa/import', 'Mahasiswa::import');
	$routes->put('mahasiswa/status/(:num)', 'Mahasiswa::setStatus/$1');
	$routes->resource('mahasiswa');
	$routes->post('closeall/(:alpha)', 'Mahasiswa::closeAll/$1');

	// CRUD User
	$routes->resource('users');
	$routes->post('users/import', 'Users::import');

	// CRUD Makul
	$routes->resource('makul');

	// CRUD Dosen
	$routes->resource('dosen');

	// CRUD Jadwal
	$routes->resource('jadwal');

	// Get Nilai
	$routes->get('nilai', 'Nilai::index');

	// Open and Close KRS
	$routes->get('krs/status/close/(:num)/(:num)', 'Krs::setStatus/$1/$2/$3');
	$routes->get('krs/status/open/(:num)/(:num)', 'Krs::setStatus/$1/$2/$3');
	$routes->post('changepassword/(:segment)', 'Auth::changePassword/$1');

});

$routes->group('dosen', ['filter' => 'dosenfilter'], function ($routes)
{

	$routes->get('mahasiswa/getbysemester/(:segment)/(:segment)', 'Mahasiswa::getBySemester/$1/$2');

	// CRUD Nilai
	$routes->get('nilai/(:num)', 'Nilai::get/$1');
	$routes->resource('nilai');

	// CRUD Materi
	$routes->get('materi/loadfile/(:num)', 'Materi::loadFile/$1');
	$routes->get('materi/(:num)/(:segment)', 'Materi::show/$1/$2');
	$routes->get('materi/(:num)', 'Materi::show/$1');
	$routes->resource('materi');

	$routes->get('dosen/(:segment)/(:segment)', 'Dosen::show/$1/$2');
	$routes->get('dosen/(:segment)', 'Dosen::show/$1');

	// Get Makul
	$routes->get('makul', 'Makul::index');
	$routes->get('makul/(:segment)/(:segment)', 'Makul::show/$1/$2');
	$routes->get('makul/(:segment)', 'Makul::show/$1');

	// Get Mahasiswa
	$routes->get('mahasiswa', 'Mahasiswa::index');

	$routes->post('changephoto/(:num)', 'Dosen::changePhoto/$1');
	$routes->post('changepassword/(:segment)', 'Auth::changePassword/$1');

});

$routes->group('mahasiswa', ['filter' => 'mahasiswafilter'], function ($routes)
{
	$routes->get('getsemester/(:num)', 'Mahasiswa::getSemester/$1');
	$routes->get('makul/(:num)/(:alpha)', 'Mahasiswa::getMakul/$1/$2');
	$routes->get('materi/(:num)/(:segment)', 'Materi::show/$1/$2');
	$routes->get('materi/loadfile/(:num)', 'Materi::loadFile/$1');
	$routes->get('cetak/khs/(:num)', 'Mahasiswa::transkipNilai/$1');
	$routes->get('cetak/krs/(:num)/(:num)/(:alpha)/(:alpha)', 'Makul::sks/$1/$2/$3/$4');
	$routes->get('cetak/(:num)/(:alpha)/(:alpha)', 'Mahasiswa::print/$1/$2/$3');
	$routes->get('kurikulum/(:alpha)', 'Makul::kurikulum/$1');
	$routes->get('dosen', 'Dosen::index');
	$routes->post('changephoto/(:num)', 'Mahasiswa::changePhoto/$1');
	$routes->get('checkstatus/(:num)', 'Mahasiswa::checkStatus/$1');

	$routes->post('changepassword/(:segment)', 'Auth::changePassword/$1');
});

$routes->environment('development', function($routes)
{
    $routes->put('mahasiswa/status/(:num)', 'Mahasiswa::setStatus/$1');
    $routes->get('mahasiswa', 'Mahasiswa::index');
	$routes->get('cetak/krs/(:num)/(:num)/(:alpha)/(:alpha)', 'Makul::sks/$1/$2/$3/$4');

	$routes->get('cetak/khs/(:num)', 'Mahasiswa::transkipNilai/$1');
	$routes->get('cetak/(:num)/(:alpha)', 'Mahasiswa::print/$1/$2');
	$routes->get('dosen', 'Dosen::index');

	$routes->post('forgotpassword/(:alpha)', 'Auth::forgotPassword/$1');

	$routes->post('resetpassword', 'Auth::resetPassword');
	$routes->post('changephoto/(:num)', 'Mahasiswa::changePhoto/$1');
	$routes->resource('mahasiswa');

	$routes->post('closeall/(:alpha)', 'Mahasiswa::closeAll/$1');

	$routes->get('checkstatus/(:num)', 'Mahasiswa::checkStatus/$1');
	$routes->get('nilai/(:num)', 'Nilai::get/$1');

	$routes->get('testpdf', 'Test::export');
	$routes->get('testdom', 'Test::dom');
	$routes->get('testmpdf', 'Test::mpd');
});

/**
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php'))
{
	require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
// CORS Access
$routes->options('(:any)', 'BaseController::cors');
