<?php
namespace App\Filters;
use CodeIgniter\Filters\FilterInterface;

use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;

use Config\Services;

Class OptionsFilter implements FilterInterface
{
	// RequestInterface $request
	public function before(RequestInterface $request, $arguments = null)
	{
		// Services::response()->setHeader("Access-Control-Allow-Headers", "X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method, Authorization");
		// Services::response()->setHeader("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, DELETE");
		// Services::response()->setHeader("Access-Control-Allow-Origin", "*");

		header('Access-Control-Allow-Origin: *');
		header("Access-Control-Allow-Headers: X-API-KEY, Authorization, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
		header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");

		$method = $_SERVER['REQUEST_METHOD'];
        if ($method == "OPTIONS") {
        	die();
        }

	}

	public function after(RequestInterface $request, ResponseInterface $response, $arguments = null)
	{
		// header('Access-Control-Allow-Origin: *');
		// header("Access-Control-Allow-Headers: X-API-KEY, Authorization, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
		// header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
	}
}
