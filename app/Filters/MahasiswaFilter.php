<?php
namespace App\Filters;

use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use CodeIgniter\Filters\FilterInterface;
use Config\Services;
use \Firebase\JWT\JWT;

class MahasiswaFilter implements FilterInterface
{

	private function _privateKey()
	{
		$privateKey = <<<EOD
			-----BEGIN RSA PRIVATE KEY-----
			MIICXAIBAAKBgQCWVSPF/cT63pA8flKiSrzTGOfP8OcBlxSiaszkD1Zeb1D/lBi2
			86e9n8Pt/hGEwpLrjjc+uOrYOIav0Xt4cK0YuoeK14aYzegGmTnTcb3I1yIJrX9W
			4b70sgIZs8DBMyFMTXYCWv75X7OazXBMn3GLSzmng0jCrqfVLq29yrQolwIDAQAB
			AoGAclGqU4zIN3H6Q1ZYpnupHeppldJ/CNGH6n7G8oLLTiLeeIx5Qt8E5wlQaHH1
			hnykVyad0j+yrtxFZ/woJY2hwuFG/PKqcpCV91T9xnZo7GndlnRiCOSBUXc4/n3I
			UzIbmBGisTpSdZ8qzRai2rbdfrqFM6WSGnScS21rcs/o0ekCQQD82v2Y+aeTSczv
			pKDt6IZc7zgmtDz132K3jgXyU5MHWRCIn6pbhPUqoSBprnpfes0PR9GvUGOtizGj
			JuL2XFN7AkEAmDO/7i3S8UseRgWXlap0wcN+RoPGsBsv/KuONNrxRH2xeJqj2io3
			pgrehz324FT74qM3bWakOTTXzshnU/EWlQJBAPDnF2vyl2RtkLHU0Ho56iGKzJ2b
			ZOUAAkZNKn84fsEmaukv7ZeqMgZ+YqotUGu9TLc9Pppf5cfG8PC3MM91IRcCQAk5
			ebRxyosdRmRwClPj1Ne5hXm4mt4ph6sEqsXBT0YbaBorJSsfy+egWQRRWRkXjXya
			780Nn35oGXxK4RhRUFECQBtMHHST6YOSJbLvxDl7DJ48dMJUmh+j+JgmEbhFMJLa
			sG/5Jm+d2jKUzD3/eMBmIYuAVg+OUik0XqxjIQW/acY=
			-----END RSA PRIVATE KEY-----
			EOD;
		return $privateKey;
	}

	private function _checkAccess(RequestInterface $request, $role = null)
	{
		$secret_key = $this->_privateKey();
		$token      = null;
		$authHeader = $request->getServer('HTTP_AUTHORIZATION');
		$arr        = explode(" ", $authHeader);
		$token      = $arr[1];

		if($token) {

			try {

				$decoded = JWT::decode($token, $secret_key, array('HS256'));

				if($decoded->data->role == $role) {
					return true;
				}

			} catch (\Exception $e) {
				return false;
			}
		}
	}

	public function before(RequestInterface $request, $arguments = null)
	{
		header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
		$token = $this->_checkAccess($request, 'mahasiswa');


		if ($token == false) {
			return Services::response()->setStatusCode(401);
		}


	}
	public function after(RequestInterface $request, ResponseInterface $response, $arguments = null)
	{
	}
}
