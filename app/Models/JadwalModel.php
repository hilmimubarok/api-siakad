<?php

namespace App\Models;

use CodeIgniter\Model;

class JadwalModel extends Model
{

	protected $table         = "jadwal";
	protected $primaryKey    = "id_jadwal";
	protected $allowedFields = ["makul_id", "mulai", "selesai", "hari", "kelas"];

}



?>
