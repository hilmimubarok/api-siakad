<?php

namespace App\Models;

use CodeIgniter\Model;

class MakulModel extends Model
{

	protected $table         = "mata_kuliah";
	protected $primaryKey    = "id_makul";
	protected $allowedFields = ["id_makul", "dosen_id", "nama_makul", "sks", "semester", "jurusan"];

}



?>
