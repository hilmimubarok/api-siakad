<?php

namespace App\Models;

use CodeIgniter\Model;

class MahasiswaModel extends Model
{

	protected $table         = "mahasiswa";
	protected $primaryKey    = "id_mahasiswa";
	protected $allowedFields = ["user_id", "nim", "nama", "foto", "email", "jenis_kelamin", "tahun_masuk", "kelas", "temp_kelas", "jurusan", "semester", "temp_semester", "status_khs", "status_krs", "status_uts", "status_uas"];

	public function findByEmail($val)
	{
		$db = \Config\Database::connect();
		$data = $db->query("SELECT * FROM mahasiswa WHERE email = '$val' ");
		$data = $data->getRow();
		if (isset($data)) {
			return $data->user_id;
		} else {
			return null;
		}
	}

}



?>
