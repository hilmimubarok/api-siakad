<?php

namespace App\Models;

use CodeIgniter\Model;

class NilaiModel extends Model
{

	protected $table         = "nilai";
	protected $primaryKey    = "id_nilai";
	protected $allowedFields = ["mahasiswa_id", "makul_id", "absen", "keaktifan", "tugas", "uts", "uas", "skor", "huruf", "bobot", "keterangan"];

}



?>
