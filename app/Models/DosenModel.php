<?php

namespace App\Models;

use CodeIgniter\Model;

class DosenModel extends Model
{

	protected $table         = "dosen";
	protected $primaryKey    = "id_dosen";
	protected $allowedFields = ["user_id", "niy", "nama_dosen", "email_dosen", "no_hp_dosen", "foto_dosen", "jenis_kelamin_dosen", "alamat_dosen", "status_dosen", "prodi_dosen"];

	public function findByEmail($val)
	{
		$db = \Config\Database::connect();
		$data = $db->query("SELECT * FROM dosen WHERE email_dosen = '$val' ");
		$data = $data->getRow();
		if (isset($data)) {
			return $data->user_id;
		} else {
			return null;
		}
	}

}



?>
